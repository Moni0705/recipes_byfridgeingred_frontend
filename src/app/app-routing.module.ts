import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: 'findIngredients', loadChildren: './pages/find-ingredients/find-ingredients.module#FindIngredientsPageModule' },
    { path: '', loadChildren: './pages/menu/menu.module#MenuPageModule' },
    /*{ path: 'menu-routing', loadChildren: './pages/menu/menu-routing.module#MenuRoutingPageModule' },
      { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
      { path: 'fridge', loadChildren: './pages/fridge/fridge.module#FridgePageModule' },
      { path: 'groceryList', loadChildren: './pages/grocery-list/grocery-list.module#GroceryListPageModule' },
      { path: 'searchRecipes', loadChildren: './pages/search-recipes/search-recipes.module#SearchRecipesPageModule' },
      { path: 'favoriteRecipes', loadChildren: './pages/favorite-recipes/favorite-recipes.module#FavoriteRecipesPageModule' },
      { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
      { path: 'privacyPolicy', loadChildren: './pages/privacy-policy/privacy-policy.module#PrivacyPolicyPageModule' }*/
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
