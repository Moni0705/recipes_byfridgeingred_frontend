import {Component, OnInit, OnChanges, DoCheck} from '@angular/core';
import {SearchRecipesService} from '../../services/search-recipes/search-recipes.service';

@Component({
    selector: 'app-search-recipes',
    templateUrl: './search-recipes.page.html',
    styleUrls: ['./search-recipes.page.scss'],
})
export class SearchRecipesPage implements OnInit, OnChanges, DoCheck {
    message: string;
    recipesArray: any;
    ingredientsForRecipes: any;
    page = 1;
    length = 0;

    constructor(public searchRecipeService: SearchRecipesService) {
        this.recipesArray = [];
        this.ingredientsForRecipes = [];
    }

    ngOnInit() {
        this.get_fridgeIngredients();
        this.length = this.ingredientsForRecipes.length;
        this.showRecipes(this.length);
    }
    ngDoCheck() {}
    ngOnChanges() {
    }

    showRecipes(length) {
        if (length === 0 ){
            this.getDefaultRecipes(this.page);
        } else {
            this.getRecipes_fridgeIngredients(this.ingredientsForRecipes[0]);
        }
    }
    getDefaultRecipes(page) {
        this.searchRecipeService.getAllRecipes(page).subscribe(recipes => {
            console.log('First 10 recipes are: ', recipes);
            this.recipesArray = recipes;
        });
    }
    get_fridgeIngredients(){
        this.searchRecipeService.currentIngredients_SearchRecipes
            .subscribe(ingredientsArray => this.ingredientsForRecipes = ingredientsArray);
    }
    getRecipes_fridgeIngredients(ingredient){
        this.searchRecipeService.getRecipesByIngredient(ingredient).subscribe(recipes => {
            this.recipesArray = recipes;
        });
    }
    refreshSearch() {
        this.recipesArray = [];
        const page = 1;
        this.length = 0;
        this.getDefaultRecipes(page);
    }
    seeRecipe(recipe) {//TODO: The title is grabbed correctly; And I need to make a request that will grab the recipe data and navigate me to the recipe page --> recipe should be taken from api by id or title?
          console.log('selected recipe is: ', recipe);
    }
    loadMoreRecipes(event) {
        this.page++;
        if (this.length === 0) {
            setTimeout(() => {
                this.searchRecipeService.getAllRecipes(this.page).subscribe(response => {
                    let incomingRecipes: any;
                    incomingRecipes = response;
                    for (let i = 0; i < incomingRecipes.length; i++){
                        this.recipesArray.push(incomingRecipes[i]);
                    }
                    event.target.complete();
                });
            }, 500);
        }

    }
}
