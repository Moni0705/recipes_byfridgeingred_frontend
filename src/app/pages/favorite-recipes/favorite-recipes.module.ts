import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FavoriteRecipesPage } from './favorite-recipes.page';

const routes: Routes = [
  {
    path: '',
    component: FavoriteRecipesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FavoriteRecipesPage]
})
export class FavoriteRecipesPageModule {}
