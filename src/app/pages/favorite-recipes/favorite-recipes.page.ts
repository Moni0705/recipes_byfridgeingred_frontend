import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favorite-recipes',
  templateUrl: './favorite-recipes.page.html',
  styleUrls: ['./favorite-recipes.page.scss'],
})
export class FavoriteRecipesPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
