import { Component, OnInit } from '@angular/core';
import {Router, RouterEvent} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {


    selectedPath = '';

    pages = [
        {
            title: 'My Fridge',
            url: '/menu/(content:fridge)'
        },
        {
            title: 'My Grocery List',
            url: '/menu/(content:grocerylist)'
        },
        {
            title: 'Search Recipes',
            url: '/menu/(content:search)'
        },
        {
            title: 'Favorite Recipes',
            url: '/menu/(content:favorite)'
        },
        {
            title: 'Account Settings',
            url: '/menu/(content:settings)'
        },
        {
            title: 'Privacy Policy',
            url: '/menu/(content:privacy)'
        }
    ];


  constructor(private router: Router) {
      this.router.events.subscribe((event: RouterEvent) => {
          this.selectedPath = event.url;
      });
  }

  /*navigateToFindPage(){
      this.selectedPath = '/menu/(content:findIngredientsFridge)';
  }*/

  ngOnInit() {}

}
