import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';
import {MenuRoutingModule} from './menu-routing.module';
import {SearchRecipesPageModule} from '../search-recipes/search-recipes.module';
import {FridgePageModule} from '../fridge/fridge.module';
import {GroceryListPageModule} from '../grocery-list/grocery-list.module';
import {SettingsPageModule} from '../settings/settings.module';
import {PrivacyPolicyPageModule} from '../privacy-policy/privacy-policy.module';
import {FavoriteRecipesPageModule} from '../favorite-recipes/favorite-recipes.module';
const routes: Routes = [
    {
        path: '',
        component: MenuPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MenuRoutingModule,
        SearchRecipesPageModule,
        FridgePageModule,
        GroceryListPageModule,
        SettingsPageModule,
        PrivacyPolicyPageModule,
        FavoriteRecipesPageModule

    ],
    declarations: [MenuPage]
})
export class MenuPageModule {}
