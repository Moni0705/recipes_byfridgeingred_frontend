import { NgModule } from '@angular/core';
import { MenuPage } from './menu.page';
import { RouterModule, Routes } from '@angular/router';
import { FridgePage } from '../fridge/fridge.page';
import { GroceryListPage } from '../grocery-list/grocery-list.page';
import { FavoriteRecipesPage } from '../favorite-recipes/favorite-recipes.page';
import { SearchRecipesPage } from '../search-recipes/search-recipes.page';
import { SettingsPage } from '../settings/settings.page';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy.page';
const routes: Routes = [
    {
        path: 'menu',
        component: MenuPage,
        children: [
            {
                path: 'fridge',
                outlet: 'content',
                component: FridgePage
            },
            {
                path: 'grocerylist',
                outlet: 'content',
                component: GroceryListPage
            },
            {
                path: 'search',
                outlet: 'content',
                component: SearchRecipesPage
            },
            {
                path: 'favorite',
                outlet: 'content',
                component: FavoriteRecipesPage
            },
            {
                path: 'settings',
                outlet: 'content',
                component: SettingsPage
            },
            {
                path: 'privacy',
                outlet: 'content',
                component: PrivacyPolicyPage
            }
        ]
    },
    {
        path: '',
        redirectTo: '/menu/(content:search)'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class MenuRoutingModule {
   /* public navigateToPage_menuRouting(){
        RouterModule.
    }*/
}
