import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MenuPage} from '../menu/menu.page';
import {FridgeService} from '../../services/fridge/fridge.service';
import {SearchRecipesService} from '../../services/search-recipes/search-recipes.service';

@Component({
    selector: 'app-fridge',
    templateUrl: './fridge.page.html',
    styleUrls: ['./fridge.page.scss'],
})
export class FridgePage implements OnInit {
    ingredientsArray: any;
    fridgeIngredientsArray: any;
    ingredientsForRecipes: any;
    private isChecked = false;

    constructor(private router: Router, private fridgeService: FridgeService, private searchRecipesServcie: SearchRecipesService) {
        this.ingredientsArray = [];
        this.fridgeIngredientsArray = [];
        this.ingredientsForRecipes = [];
    }

    ngOnInit() {
        this.fridgeService.currentFridgeIngredients
            .subscribe(ingredientsArray => this.ingredientsArray = ingredientsArray);
        this.searchRecipesServcie.currentIngredients_SearchRecipes
            .subscribe(ingredientsArray => this.ingredientsForRecipes = ingredientsArray);
    }
    searchForIngredient(){
      this.router.navigateByUrl('findIngredients');
    }
    selectedIngredientsForSearch(selectedIngredient, event) {
        this.isChecked = event.detail.checked;
        if (this.isChecked) {
            this.ingredientsForRecipes.push(selectedIngredient);
        } else {
            this.ingredientsForRecipes.pop(selectedIngredient);
        }
        this.searchRecipesServcie.getFridgeIngredients(this.ingredientsForRecipes);
    }
}
