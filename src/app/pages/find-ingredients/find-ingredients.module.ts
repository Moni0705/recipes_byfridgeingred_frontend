import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FindIngredientsPage } from './find-ingredients.page';

const routes: Routes = [
  {
    path: '',
    component: FindIngredientsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FindIngredientsPage]
})
export class FindIngredientsPageModule {}
