import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {SearchIngredientsService} from '../../services/search-ingredients/search-ingredients.service';
import {FridgeService} from '../../services/fridge/fridge.service';
import { Location } from '@angular/common';

@Component({
    selector: 'app-find-ingredients',
    templateUrl: './find-ingredients.page.html',
    styleUrls: ['./find-ingredients.page.scss'],
})
export class FindIngredientsPage implements OnInit {
    private ingredientsArray: any;
    private isChecked: false;
    fridgeIngredientsArray: any;

    constructor(private searchIngredientsService: SearchIngredientsService, private fridgeService: FridgeService, private location: Location) {
        this.fridgeIngredientsArray = [];
    }

    ngOnInit() {
        this.fridgeService.currentFridgeIngredients.subscribe(ingredientsArray => this.fridgeIngredientsArray = ingredientsArray);
    }
    first(event) {
        return new Promise(function(resolve, reject) {
            let ingredient = null;
            setTimeout(() => {
                ingredient = event.detail.value.trim();
                if (!ingredient) {
                    return null;
                }
                resolve(ingredient);
            } ,3000);
        });
    }
    async onChange(event) {
        let result = await this.first(event);
        /*this.ingredientsArray = ['potato', 'onion', 'flour'];*/
        this.searchIngredientsService.findIngredient(result).subscribe(response => {
            this.ingredientsArray = response;
        });
    }
    checkboxOnChange(ingredient, event) {
        this.isChecked = event.detail.checked;
        if (this.isChecked) {
            this.fridgeIngredientsArray.push(ingredient);
        } else {
            this.fridgeIngredientsArray.pop(ingredient);
        }
        this.fridgeService.getFridgeIngredients(this.fridgeIngredientsArray);
    }
    getPreviousPage(){
        this.location.back();
    }

}
