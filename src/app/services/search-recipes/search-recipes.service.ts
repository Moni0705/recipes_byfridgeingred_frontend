import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject, from} from 'rxjs/index';
import {forEach} from '@angular/router/src/utils/collection';


@Injectable({
    providedIn: 'root'
})
export class SearchRecipesService {

    private fridgeIngredientsSource = new BehaviorSubject<any>([]);
    public currentIngredients_SearchRecipes = this.fridgeIngredientsSource.asObservable();

    constructor(private http: HttpClient) { }
    getFridgeIngredients(ingredients: []) {
        this.fridgeIngredientsSource.next(ingredients);
    }
    getAllRecipes(pageNumber) {
        if (!pageNumber)
            pageNumber = 0;
        return this.http.get(`http://localhost:3333/recipes?pageNumber=${pageNumber}`);
    }
    getRecipesByIngredient(ingredient) {
        console.log('I am ingredients array: ', ingredient); // I need to make it take multi  ingredients too :)
        return this.http.get(`http://localhost:3333/recipes/search/${ingredient}`);
    }

}