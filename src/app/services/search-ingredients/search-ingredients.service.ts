import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchIngredientsService {

  constructor(private http: HttpClient) { }

    findIngredient(ingredient) {
        if (!ingredient)
            return;
        return this.http.get(`http://localhost:3333/ingredients/${ingredient}`);
    }
}
