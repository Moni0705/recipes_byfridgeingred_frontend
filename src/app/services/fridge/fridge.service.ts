import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class FridgeService {

  private fridgeIngredientsSource = new BehaviorSubject<any>([]);
  public currentFridgeIngredients = this.fridgeIngredientsSource.asObservable();

  constructor() { }

  getFridgeIngredients(ingredients: []) {
    this.fridgeIngredientsSource.next(ingredients);
  }

}
