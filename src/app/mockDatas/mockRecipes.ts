import {Recipe} from './Recipe';

export const Recipes: Recipe[] = [
    { image: 'TestImage',
        title: 'TestImage',
        description: 'TestImage',
        missingIngredients: 'TestImage'
    },
    { image: 'TestImage',
        title: 'TestImage',
        description: 'TestImage',
        missingIngredients: 'TestImage'
    },
    { image: 'TestImage',
        title: 'TestImage',
        description: 'TestImage',
        missingIngredients: 'TestImage'
    },
    { image: 'TestImage',
        title: 'TestImage',
        description: 'TestImage',
        missingIngredients: 'TestImage'
    }
];