export class Recipe {
    image: string;
    title: string;
    description: string;
    missingIngredients: string;

}
